import pytest
from box import Box, BoxList
from mock import MagicMock


@pytest.fixture(autouse=True)
def conf(mocker, tmp_path):

    # Mock current settings
    mock_settings = MagicMock()
    mocked_config = Box(
        {
            "name": "default",
            "description": "default context",
            "instance": "https://jira.local",
            "default_project": "JIRA",
            "plugin": "jira",
            "login": "mylogin",
            "password": "mypassword",
            "custom_fields.story_points": "customfield_10000",
            "projects.JIRA.workflow": {"new": BoxList(["1"]), "indeterminate": BoxList(["2"]), "done": BoxList(["3"])},
            "custom_fields.peer": "customfield_10001",
        }
    )
    mock_settings.return_value.__getitem__.side_effect = mocked_config.__getitem__
    mock_settings.return_value.get = mocked_config.__getitem__

    # Mock global_settings
    mock_global_settings = MagicMock()
    mocked_global_settings = {"current_context": "default", "contexts": [mocked_config]}
    mock_global_settings.return_value.__getitem__.side_effect = mocked_global_settings.__getitem__
    mock_global_settings.return_value.get = mocked_global_settings.__getitem__

    # Mock configuration in moic.cli
    mocker.patch("moic.base.settings", new=mock_settings())
    mocker.patch("moic.cli.base.global_settings", new=mock_global_settings())
    mocker.patch("moic.cli.base.settings", new=mock_settings())
    mocker.patch("moic.cli.commands.context.base.global_settings", new=mock_global_settings())
    mocker.patch("moic.cli.commands.issue.base.global_settings", new=mock_global_settings())
    mocker.patch("moic.cli.commands.issue.base.settings", new=mock_settings())
    mocker.patch("moic.cli.commands.resources.base.global_settings", new=mock_global_settings())
    mocker.patch("moic.plugins.jira.base.global_settings", new=mock_global_settings())
    mocker.patch("moic.plugins.jira.base.settings", new=mock_settings())
    mocker.patch("moic.plugins.jira.commands.sprint.base.global_settings", new=mock_global_settings())
    mocker.patch("moic.plugins.jira.commands.sprint.base.settings", new=mock_settings())
    mocker.patch("moic.plugins.jira.utils.base.settings", new=mock_settings())

    # Mocking instance up probe
    mocker.patch("moic.base.check_instance_is_up")

    # Mocking conf_dir
    mocker.patch("moic.cli.commands.context.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.cli.commands.template.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.cli.utils.base.CONF_DIR", new=tmp_path)
