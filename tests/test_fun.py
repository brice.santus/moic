import pytest
from click.testing import CliRunner

from moic.base import cli


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


def test_rabbit(mocker, runner):
    command_args = ["rabbit"]
    result = runner.invoke(cli, command_args)

    assert result.exit_code == 0
