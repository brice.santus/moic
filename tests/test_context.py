import json

import pytest
from box import BoxList
from click.testing import CliRunner
from mock import MagicMock

from moic.base import cli


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


def test_default_config(mocker, runner, capfd, tmp_path):
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    from moic.cli import MoicInstance

    MoicInstance()
    assert open(f"{tmp_path}/config.yaml", "r").read() == "default:\n  contexts: []\n  current_context: ''\n"


def test_jira_instance(mocker, capfd, tmp_path):

    new_config = ["https://jira.local", "mylogin", "mypassword", "JIRA"]
    mock_jira = MagicMock()
    mock_prompt = MagicMock(side_effect=new_config)
    mock_get_password = MagicMock()
    mock_get_password.return_value = "mypassword"
    mocker.patch("moic.plugins.jira.base.JIRA", new=mock_jira)
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("click.prompt", new=mock_prompt)
    mocker.patch("click.confirm", new=MagicMock(return_value=True))
    mocker.patch("keyring.set_password")
    mocker.patch("keyring.get_password", new=mock_get_password)

    from moic.plugins.jira import Instance

    jira = Instance()
    Instance.instance = None
    assert isinstance(jira.session, MagicMock)
    mock_jira.assert_called_once_with(
        "https://jira.local",
        basic_auth=("mylogin", "mypassword"),
        options={"agile_rest_path": "agile", "rest_api_version": "latest", "agile_rest_api_version": "latest"},
    )


def test_add_context_succeeded(mocker, runner, capfd, tmp_path):

    new_config = ["https://jira.local", "mylogin", "mypassword", "JIRA"]
    mock_jira = MagicMock()
    mock_prompt = MagicMock(side_effect=new_config)
    mocker.patch("moic.plugins.jira.base.JIRA", new=mock_jira)
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("click.prompt", new=mock_prompt)
    mocker.patch("click.confirm", side_effect=[True, False])
    mocker.patch("keyring.set_password")
    mocker.patch("moic.cli.base.MoicInstance.set_current_context")

    command_args = ["context", "add", "jira", "--name", "jira", "--description", "New Jira config"]
    result = runner.invoke(cli, command_args)
    out, err = capfd.readouterr()
    output = "\n > Configuration stored in "

    assert result.exit_code == 0
    assert out.startswith(output)


def test_add_jira_context_with_agile_succeeded(mocker, runner, capfd, tmp_path):
    new_config = ["https://jira.local", "mylogin", "mypassword", "JIRA", "customfield_10001", "customfield_10000"]
    mock_jira = MagicMock()
    mock_prompt = MagicMock(side_effect=new_config)
    mocker.patch("moic.plugins.jira.base.JIRA", new=mock_jira)
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("click.prompt", new=mock_prompt)
    mocker.patch("click.confirm", side_effect=[True, True])
    mocker.patch("keyring.set_password")
    mocker.patch("moic.cli.base.MoicInstance.set_current_context")
    mock_jira = MagicMock()
    fields = [
        {
            "id": "customfield_10000",
            "name": "Story Points",
            "custom": True,
            "orderable": True,
            "navigable": True,
            "searchable": True,
            "clauseNames": ["cf[10000]", "Story Points"],
            "schema": {
                "type": "number",
                "custom": "com.atlassian.jira.plugin.system.customfieldtypes:float",
                "customId": 10000,
            },
        },
        {
            "id": "customfield_10001",
            "name": "Peer",
            "custom": True,
            "orderable": True,
            "navigable": True,
            "searchable": True,
            "clauseNames": ["cf[10001]", "Peer"],
            "schema": {
                "type": "number",
                "custom": "com.atlassian.jira.plugin.system.customfieldtypes:float",
                "customId": 10001,
            },
        },
    ]
    mock_jira.fields = MagicMock(return_value=fields)
    mock_config = MagicMock()
    mocked_config = {
        "instance": "https://jira.local",
        "plugin": "jira",
        "project": "JIRA",
        "login": "mylogin",
        "password": "mypassword",
        "custom_fields.story_points": "customfield_10000",
        "projects.JIRA.workflow": {"new": BoxList(["1"]), "indeterminate": BoxList(["2"]), "done": BoxList(["3"])},
        "custom_fields.peer": "customfield_10001",
    }
    mock_config.return_value.__getitem__.side_effect = mocked_config.__getitem__
    mock_config.return_value.get = mocked_config.__getitem__
    mocker.patch("moic.plugins.jira.base.settings", new=mock_config())
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    mock_get_password = MagicMock()
    mock_get_password.return_value = "mypassword"
    mocker.patch("keyring.get_password", new=mock_get_password)
    mock_request = MagicMock()
    mock_request.return_value.text = json.dumps(
        [
            {
                "name": "Story",
                "statuses": [
                    {
                        "description": "",
                        "name": "To Do",
                        "id": "1",
                        "statusCategory": {"id": 2, "key": "new", "colorName": "blue-gray", "name": "To Do"},
                    },
                    {
                        "description": "This issue is being actively worked on at the moment by the assignee.",
                        "name": "In Progress",
                        "id": "2",
                        "statusCategory": {
                            "id": 4,
                            "key": "indeterminate",
                            "colorName": "yellow",
                            "name": "In Progress",
                        },
                    },
                    {
                        "description": "Validation, Testing, Demo...",
                        "name": "QA",
                        "id": "3",
                        "statusCategory": {
                            "id": 4,
                            "key": "indeterminate",
                            "colorName": "yellow",
                            "name": "In Progress",
                        },
                    },
                    {
                        "description": "",
                        "name": "Done",
                        "id": "4",
                        "statusCategory": {"id": 3, "key": "done", "colorName": "green", "name": "Done"},
                    },
                ],
            }
        ]
    )
    mocker.patch("requests.request", new=MagicMock(return_value=mock_request()))
    mocker.patch(
        "click.edit", new=MagicMock(return_value="{'new': ['1'], 'indeterminate': ['2', '3'], 'done': ['4']}"),
    )

    command_args = ["context", "add", "jira", "--name", "jira", "--description", "New Jira config"]
    result = runner.invoke(cli, command_args)
    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert (
        out
        == f"\n\nAgile configuration: custom fields for peer is not set\n - customfield_10001   : Peer\n\nAgile configuration: custom fields for story points is not set\n - customfield_10000   : Story Points\n\nAgile configuration:  workflow is not set\n{{'done': ['4'], 'indeterminate': ['2', '3'], 'new': ['1']}}\n\nAgile workflow saved!\n > Configuration stored in \n{tmp_path}/config.yaml\n"
    )


def test_list_context(mocker, runner, capfd, tmp_path):

    mock_global_settings = MagicMock()
    mocked_global_settings = {"current_context": "default", "contexts": []}
    mock_global_settings.return_value.__getitem__.side_effect = mocked_global_settings.__getitem__
    mock_global_settings.return_value.get = mocked_global_settings.__getitem__
    mocker.patch("moic.cli.commands.context.base.global_settings", new=mock_global_settings())

    command_args = ["context", "list"]
    result = runner.invoke(cli, command_args)
    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "No context defined yet\n> Please run 'moic context add' to setup configuration\n\n"

    mock_global_settings = MagicMock()
    mocked_global_settings = {
        "current_context": "default",
        "contexts": [{"name": "mycontext", "description": "This is a context"}],
    }
    mock_global_settings.return_value.__getitem__.side_effect = mocked_global_settings.__getitem__
    mock_global_settings.return_value.get = mocked_global_settings.__getitem__
    mocker.patch("moic.cli.commands.context.base.global_settings", new=mock_global_settings())

    command_args = ["context", "list"]
    result = runner.invoke(cli, command_args)
    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "• mycontext           : This is a context\n"
