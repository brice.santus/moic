def test_get_issuers(mocker, tmp_path):
    from moic.cli.completion import autocomplete_plugins

    res = autocomplete_plugins("", "", "ji")
    assert res == ["jira"]
