import pkg_resources
import pytest
from box import Box
from click.testing import CliRunner
from mock import MagicMock

from moic.base import cli


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


def test_sort_issue_per_status(mocker):
    from moic.cli.utils.base import sort_issue_per_status

    mocked_issue_1 = MagicMock()
    mocked_issue_1.status.id = "2"
    mocked_issue_2 = MagicMock()
    mocked_issue_2.status.id = "3"
    mocked_issue_3 = MagicMock()
    mocked_issue_3.status.id = "1"
    issue_list = [mocked_issue_1, mocked_issue_2, mocked_issue_3]
    sorted_issue_list = sort_issue_per_status(issue_list, project="JIRA")
    assert sorted_issue_list == [mocked_issue_3, mocked_issue_1, mocked_issue_2]

    # Check raises if plugin doesn't exists
    mock_settings = MagicMock()
    mocked_config = Box({"plugin": "noplugin"})
    mock_settings.return_value.get = mocked_config.__getitem__
    mocker.patch("moic.cli.utils.base.settings", new=mock_settings())
    assert issue_list == sort_issue_per_status(issue_list, project="JIRA")


def test_get_template(mocker):
    from moic.cli.utils.base import get_template

    mocker.patch(
        "os.path.isfile", new=MagicMock(side_effect=[True, False, True, False, False, True, False, False, False])
    )

    assert get_template("project", "story").endswith("/templates/project_story")
    assert get_template("project", "story").endswith("/templates/project_story")
    assert get_template("project", "story").endswith("/templates/all_all")
    assert get_template("project", "story") is None


def test_merge_config():
    from moic.cli.base import merge_config

    a = {"contexts": [{"name": "toto", "value": "2", "workflow": {"done": ["3"], "progress": ["3"]}}]}
    b = {"contexts": [{"name": "toto", "value": "3", "other": "nop", "workflow": {"done": ["1", "2"]}}]}
    assert merge_config(a, b) == {
        "contexts": [
            {"name": "toto", "value": "3", "workflow": {"done": ["1", "2"], "progress": ["3"]}, "other": "nop"}
        ]
    }


def test_get_plugin_command_failed(mocker, capfd):
    from moic.cli.utils.base import get_plugin_command

    exit_mock = MagicMock()
    mocker.patch("moic.cli.utils.base.exit", new=exit_mock())
    # test command not implemented
    get_plugin_command("issue", "nosub")
    out, err = capfd.readouterr()
    assert out == "Plugin jira issue command doesn't provide a nosub subcommand\n"
    # test plugin not existing
    mock_settings = MagicMock()
    mocked_config = Box({"plugin": "noplugin"})
    mock_settings.return_value.get = mocked_config.__getitem__
    mocker.patch("moic.cli.utils.base.settings", new=mock_settings())
    get_plugin_command("issue", "get")
    out, err = capfd.readouterr()
    assert out == "Plugin noplugin doesn't provide a issue command\n"


def test_get_plugin_custom_command_no_plugin(mocker, capfd):
    from moic.cli.utils.base import get_plugin_custom_commands

    exit_mock = MagicMock()
    mocker.patch("moic.cli.utils.base.exit", new=exit_mock())
    # test plugin not existing
    get_plugin_custom_commands("noplugin")
    out, err = capfd.readouterr()

    assert out == "Plugin noplugin doesn't exists\n"
    # test command not implemented


def test_get_plugin_custom_command_failed(mocker, capfd):
    from moic.cli.utils.base import get_plugin_custom_commands

    exit_mock = MagicMock()
    mocker.patch("moic.cli.utils.base.exit", new=exit_mock())
    mocker.patch("moic.cli.utils.base.getattr", new=MagicMock(return_value=[]))
    assert [] == get_plugin_custom_commands("jira")

    mocker.patch("moic.cli.utils.base.getattr", new=MagicMock(side_effect=[["issue.toto"], AttributeError]))

    get_plugin_custom_commands("jira")
    out, err = capfd.readouterr()
    assert out == "Plugin jira doesn't provide a custom subcommand toto for command issue\n"


def test_version(runner, capfd):
    command_args = ["version"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == f"Moic version: {pkg_resources.get_distribution('moic').version}\n"
