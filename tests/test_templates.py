import pytest
from click.testing import CliRunner
from mock import MagicMock

from moic.base import cli


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


@pytest.fixture(autouse=True)
def default_mock(mocker, tmpdir):
    p = tmpdir.mkdir("templates").join("JIRA_Story")
    p.write("h1. Story template")
    mocker.patch("moic.cli.commands.template.base.CONF_DIR", new=tmpdir)


def test_edit_template_succeeded(mocker, runner, capfd, tmpdir):
    mock_edit = MagicMock()
    mock_edit.side_effect = lambda a: a

    mocker.patch("click.edit", new=mock_edit)
    command_args = ["template", "edit"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    mock_edit.assert_called_once_with("Create your template here\n")
    # Got a \n here cause of console.print
    # console.print is limited to 80 char
    path = f"{tmpdir}/templates"[0:79]
    assert out == f"Your template has been saved in \n{path}\n"


def test_edit_template_with_params_succeeded(mocker, runner, capfd, tmpdir):
    mock_edit = MagicMock()

    mocker.patch("click.edit", new=mock_edit)
    command_args = ["template", "edit", "--project", "JIRA", "--issue-type", "Story"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    mock_edit.assert_called_once_with(filename=f"{tmpdir}/templates/JIRA_Story")
    # Got a \n here cause of console.print
    # console.print is limited to 80 char
    path = f"{tmpdir}/templates"[0:80]
    assert out == f"Your template has been saved in \n{path}\n"


def test_list_template_succeeded(mocker, runner, capfd, tmpdir):

    command_args = ["template", "list"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    # Got a \n here cause of console.print
    # console.print is limited to 80 char
    assert out.startswith("Story     JIRA      : ")
    assert "templates/JIRA_Story" in out
