import pytest
from click.testing import CliRunner
from mock import MagicMock

from moic.base import cli


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


def issue_output(
    issue_key,
    issue_type,
    issue_status,
    issue_summary,
    issue_reporter,
    issue_assignee,
    issue_peer,
    issue_output_header,
    oneline=False,
    linejump=False,
    subtask=False,
):
    output = ""
    output_key = f"key             : {issue_key}\n"
    output_type = f"type            : {issue_type}\n"
    output_status = f"status          : {issue_status}\n"
    output_summary = f"summary         : {issue_summary}\n"
    output_reporter = f"reporter        : {issue_reporter}\n"
    output_assignee = f"assignee        : {issue_assignee}\n"
    output_peer = f"peer            : {issue_peer}\n"
    output_link = f"link            : https://jira.local/browse/{issue_key}\n"
    if not oneline:
        output_subtaks = "subtasks        :\n"
        output_subtaks = "subtasks        :\n  └─  To Do     | JIRA-sub subtask https://jira.local/browse/JIRA-sub\n"
    else:
        output_subtaks = "    └─ JIRA-sub subtask https://jira.local/browse/JIRA-sub\n"

    issue_output_header = issue_output_header + "\n" if issue_output_header else ""
    if oneline:
        output = f"{issue_key} {issue_summary} https://jira.local/browse/{issue_key}\n"
    else:
        output = (
            issue_output_header
            + output_key
            + output_type
            + output_status
            + output_summary
            + output_reporter
            + output_assignee
            + output_peer
            + output_link
        )
    if linejump:
        output = "\n" + output
    if subtask:
        output = output + output_subtaks
    return output


@pytest.fixture(autouse=True)
def default_mock(mocker):
    mocker.patch("moic.plugins.jira.Instance.add_context")
    mocker.patch("moic.plugins.jira.Instance.setup_home_dir")


def test_get_missing_issue_key(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    command_args = ["issue", "get"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "You must specify an issue ID or a search query\n"


@pytest.mark.parametrize(
    "issue_key, issue_type, issue_desc, issue_summary, issue_status, issue_reporter, issue_assignee, issue_peer, template_name, template, issue_output_header, oneline, search",
    [
        (
            "JIRA-1",
            "Story",
            "This is a description",
            "This is the summary",
            "To Do",
            "Pierre Tilendier",
            "",
            "",
            "",
            "",
            "",
            False,
            "",
        ),
        (
            "JIRA-1",
            "Story",
            "This is a description",
            "This is the summary",
            "To Do",
            "Pierre Tilendier",
            "",
            "",
            "",
            "",
            "",
            True,
            "",
        ),
        (
            "JIRA-1",
            "Story",
            "This is a description",
            "This is the summary",
            "To Do",
            "Pierre Tilendier",
            "",
            "",
            "",
            "",
            "",
            False,
            "summary ~ 'summary'",
        ),
    ],
)
def test_get_succeeded(
    mocker,
    runner,
    capfd,
    issue_key,
    issue_type,
    issue_desc,
    issue_summary,
    issue_status,
    issue_reporter,
    issue_assignee,
    issue_peer,
    template_name,
    template,
    issue_output_header,
    oneline,
    search,
):

    mock_jira = MagicMock()
    # Mock status
    mock_status = MagicMock()
    mock_status.return_value.id = "1"
    mock_status.return_value.name = issue_status
    mock_status.return_value.statusCategory.colorName = "blue"
    # Mock issue
    mock_issue = MagicMock()
    mock_issue.return_value.key = issue_key
    mock_issue.return_value.fields.issuetype.name = issue_type
    mock_issue.return_value.fields.summary = issue_summary
    mock_issue.return_value.fields.reporter = issue_reporter
    mock_issue.return_value.fields.assignee = issue_assignee
    mock_issue.return_value.peer = issue_peer
    mock_issue.return_value.fields.status = mock_status()
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.search_issues = MagicMock(return_value=[mock_issue()])
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    # Mock subtassk
    mock_subtask = MagicMock()
    mock_subtask.return_value.key = "JIRA-sub"
    mock_subtask.return_value.fields.status = mock_status()
    mock_subtask.return_value.fields.summary = "subtask"
    mock_issue.return_value.fields.subtasks = [mock_subtask()]
    mocker.patch(
        "moic.cli.utils.base.sort_issue_per_status", new=MagicMock(side_effect=lambda a: a),
    )

    command_args = ["issue", "get"]
    if search:
        command_args.append("--search")
        command_args.append(search)
    else:
        command_args.append(issue_key)
    if oneline:
        command_args.append("--oneline")
    command_args.append("--subtasks")
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    output = issue_output(
        issue_key,
        issue_type,
        issue_status,
        issue_summary,
        issue_reporter,
        issue_assignee,
        issue_peer,
        issue_output_header,
        oneline=oneline,
        linejump=not oneline,
        subtask=True,
    )
    assert out == output


@pytest.mark.parametrize(
    "issue_key, issue_type, issue_desc, issue_summary, issue_status, issue_reporter, issue_assignee, issue_peer, template_name, template, issue_output_header",
    [
        (
            "JIRA-1",
            "Story",
            "This is a description",
            "This is the summary",
            "To Do",
            "Pierre Tilendier",
            "",
            "",
            "",
            "",
            "",
        ),
        (
            "JIRA-1",
            "Story",
            "This is a description",
            "This is the summary",
            "To Do",
            "Pierre Tilendier",
            "",
            "",
            "all_all",
            "h1. Demo Template",
            "Using default template of all project (all_all)",
        ),
    ],
)
def test_add_issue_succeeded(
    mocker,
    runner,
    capfd,
    issue_key,
    issue_type,
    issue_desc,
    issue_summary,
    issue_status,
    issue_reporter,
    issue_assignee,
    issue_peer,
    template_name,
    template,
    issue_output_header,
):

    mock_jira = MagicMock()

    mock_edit = MagicMock()
    mock_edit.side_effect = lambda a: a + f"\n\n{issue_desc}"

    mock_status = MagicMock()
    mock_status.return_value.raw = {"statusCategory": {"colorName": "blue"}}
    mock_jira.status = MagicMock(return_value=mock_status())

    # Mock status
    mock_status_inprogress = MagicMock()
    mock_status_inprogress.return_value.id = "2"
    mock_status_inprogress.return_value.name = issue_status
    mock_status_inprogress.return_value.statusCategory.colorName = "yellow"
    # Mock issue
    mock_new_issue = MagicMock()
    mock_new_issue.return_value.key = issue_key
    mock_new_issue.return_value.fields.issuetype.name = issue_type
    mock_new_issue.return_value.fields.summary = issue_summary
    mock_new_issue.return_value.fields.reporter = issue_reporter
    mock_new_issue.return_value.fields.assignee = issue_assignee
    mock_new_issue.return_value.peer = issue_peer
    mock_new_issue.return_value.fields.status = mock_status_inprogress()
    mock_jira.create_issue = MagicMock(return_value=mock_new_issue())
    # Mock issue types
    mock_issue_type = MagicMock()
    mock_issue_type.return_value.name = "Story"
    mock_jira.issue_types = MagicMock(return_value=[mock_issue_type()])

    # Set template
    if template_name:
        mock_open = MagicMock()
        mock_open.side_effect = [mock_open(read_data=template).return_value]
        mocker.patch("moic.plugins.jira.commands.issue.base.open", new=mock_open)

    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    mocker.patch("moic.plugins.jira.commands.issue.base.get_template", new=MagicMock(return_value=template_name))
    mocker.patch("click.edit", new=mock_edit)

    command_args = ["issue", "add", issue_summary]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == issue_output(
        issue_key,
        issue_type,
        issue_status,
        issue_summary,
        issue_reporter,
        issue_assignee,
        issue_peer,
        issue_output_header,
    )


@pytest.mark.parametrize(
    "issue_key, issue_desc, issue_summary, issue_status, issue_assignee, issue_reporter, template_name, template, issue_output_header",
    [("JIRA-1", "This is a description", "This is the summary", "To Do", "", "", "", "", "",)],
)
def test_add_issue_failed(
    mocker,
    runner,
    capfd,
    issue_key,
    issue_desc,
    issue_summary,
    issue_status,
    issue_assignee,
    issue_reporter,
    template_name,
    template,
    issue_output_header,
):

    mock_jira = MagicMock()

    mock_edit = MagicMock()
    mock_edit.side_effect = lambda a: a + f"\n\n{issue_desc}"

    # Mock status
    mock_status = MagicMock()
    mock_status.return_value.raw = {"statusCategory": {"colorName": "blue"}}
    mock_jira.status = MagicMock(return_value=mock_status())

    # Mock issue
    mock_new_issue = MagicMock()
    mock_new_issue.return_value.key = issue_key
    mock_new_issue.return_value.fields.summary = issue_summary
    mock_new_issue.return_value.fields.reporter = issue_assignee
    mock_new_issue.return_value.fields.assignee = issue_reporter
    mock_new_issue.return_value.fields.status = issue_status

    # Mock issue types
    mock_issue_type = MagicMock()
    mock_issue_type.return_value.name = "Story"
    mock_jira.issue_types = MagicMock(return_value=[mock_issue_type()])

    mock_create_issue = MagicMock()
    mock_create_issue.side_effect = Exception("Boom")
    mock_jira.create_issue = mock_create_issue

    # Set template
    if template_name:
        mock_open = MagicMock()
        mock_open.side_effect = [mock_open(read_data=template).return_value]
        mocker.patch("moic.plugins.jira.commands.issue.base.open", new=mock_open)

    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    mocker.patch("moic.plugins.jira.commands.issue.base.get_template", new=MagicMock(return_value=template_name))
    mocker.patch("click.edit", new=mock_edit)

    command_args = ["issue", "add", issue_summary]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Something goes wrong Boom\n"


def test_comment_succeeded(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_add_comment = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_edit = MagicMock(return_value="This is a new comment")

    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.add_comment = mock_add_comment

    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    mocker.patch("click.edit", new=mock_edit)

    result = runner.invoke(cli, ["issue", "comment", "JIRA-1"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    mock_edit.assert_called_with("")
    mock_add_comment.assert_called_once_with("JIRA-1", "This is a new comment")

    result = runner.invoke(cli, ["issue", "comment", "JIRA-1", "--comment", "This is another comment"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    mock_edit.assert_called_once()
    mock_add_comment.assert_called_with("JIRA-1", "This is another comment")


def test_comment_failed(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_add_comment = MagicMock()
    mock_add_comment.side_effect = Exception("Boom")
    mock_edit = MagicMock(return_value="This is a new comment")
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.add_comment = mock_add_comment

    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    mocker.patch("click.edit", new=mock_edit)

    result = runner.invoke(cli, ["issue", "comment", "JIRA-1"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Something goes wrong Boom\n"


def test_assign_succeeded(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_user = MagicMock()
    mock_user.return_value.name = "ptilendier"
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.user = MagicMock(return_value=mock_user())
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    result = runner.invoke(cli, ["issue", "assign", "JIRA-2", "ptilendier"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Assigned ptilendier on JIRA-1\n"


def test_assign_failed(mocker, runner, capfd):
    class mock_issue:
        key = "JIRA-1"

    mock_jira = MagicMock()
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_update = MagicMock()
    mock_update.side_effect = Exception("Boom")
    mock_issue.update = mock_update
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    result = runner.invoke(cli, ["issue", "assign", "JIRA-1", "ptilendier"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Something goes wrong Boom\n"


def test_set_peer_succeeded(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_user = MagicMock()
    mock_user.return_value.name = "ptilendier"
    mock_jira.user = MagicMock(return_value=mock_user())
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    result = runner.invoke(cli, ["issue", "set-peer", "JIRA-2", "ptilendier"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Peer ptilendier added on JIRA-1\n"


def test_set_peer_failed(mocker, runner, capfd):
    class mock_issue:
        key = "JIRA-1"

    mock_jira = MagicMock()
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_update = MagicMock()
    mock_update.side_effect = Exception("Boom")
    mock_issue.update = mock_update
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    result = runner.invoke(cli, ["issue", "set-peer", "JIRA-1", "ptilendier"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Something goes wrong Boom\n"


def test_move_succeeded(mocker, runner, capfd):
    class mock_issue:
        key = "JIRA-1"

    transition = {
        "id": 1,
        "name": "In Progress",
        "to": {"statusCategory": {"colorName": "blue"}},
    }

    mock_jira = MagicMock()
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.transitions = MagicMock(return_value=[transition])
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    result = runner.invoke(cli, ["issue", "move", "JIRA-1", "In Progress"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Moved JIRA-1 to In Progress status\n"


def test_move_not_available(mocker, runner, capfd):
    class mock_issue:
        key = "JIRA-1"

    mock_jira = MagicMock()
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.transitions = MagicMock(return_value=[])
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    result = runner.invoke(cli, ["issue", "move", "JIRA-1", "In Progress"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "No transition available for JIRA-1\n"


def test_move_failed(mocker, runner, capfd):
    class mock_issue:
        key = "JIRA-1"

    transition = {
        "id": 1,
        "name": "In Progress",
        "to": {"statusCategory": {"colorName": "blue"}},
    }

    mock_jira = MagicMock()
    mock_apply_transition = MagicMock()
    mock_apply_transition.side_effect = Exception("Boom")

    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.transitions = MagicMock(return_value=[transition])
    mock_jira.transition_issue = mock_apply_transition
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    result = runner.invoke(cli, ["issue", "move", "JIRA-1", "In Progress"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Something goes wrong Boom\n"


def test_show_issue(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_issue.return_value.fields.description = "h1. Header\r\nh2. Header\r\n\r\n* item 1\r\n** item 2\r\n\r\nThis is text with => arrow by [~ptilendier]\r\n\r\n{code:python}\r\nimport moic\r\n\r\nmoic.rabbit()\r\n{code}\r\nWith a *bold* and _italic_ {{monospaced}} text\r\ntalking about [Google|https://google.com] or [htts://gitlab.com]\r\nThis is -amazing- but +surprising+"
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    result = runner.invoke(cli, ["issue", "show", "JIRA-1"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert (
        out
        == "Header\nHeader\n\n• item 1\n  • item 2\n\nThis is text with => arrow by @ptilendier\n\nimport moic                                                                     \n                                                                                \nmoic.rabbit()                                                                   \nWith a bold and italic monospaced text\ntalking about Google | https://google.com or htts://gitlab.com\nThis is amazing but surprising\n"
    )


def test_list_comments_issue(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_comment = MagicMock()
    mock_comment.return_value.id = 1
    mock_comment.return_value.author.displayName = "Pierre Tilendier"
    mock_comment.return_value.created = "2020-03-15T15:00:41.133+0100"
    mock_comment.return_value.body = "This is a comment"
    mock_jira.comments = MagicMock(return_value=[mock_comment()])
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    result = runner.invoke(cli, ["issue", "list-comments", "JIRA-1"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "1 Pierre Tilendier - 03/15/2020 15:00:41\n\nThis is a comment\n\n"


def test_edit_issue(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_issue.return_value.fields.description = "This is a content"
    mo = mock_issue()
    mock_jira.issue = MagicMock(return_value=mo)
    edit = MagicMock(return_value="This is a new content")
    mocker.patch("click.edit", new=edit)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    result = runner.invoke(cli, ["issue", "edit", "JIRA-1"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Issue updated!\n"
    edit.assert_called_once_with("This is a content")
    mo.update.assert_called_once_with(description="This is a new content")


def test_edit_comment(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_comment = MagicMock()
    mock_comment.return_value.id = 1
    mock_comment.return_value.author.displayName = "Pierre Tilendier"
    mock_comment.return_value.created = "2020-03-15T15:00:41.133+0100"
    mock_comment.return_value.body = "This is a comment"
    mo = mock_comment()
    mock_jira.comment = MagicMock(return_value=mo)
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mocker.patch("click.edit", new=MagicMock(return_value="New comment"))
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    result = runner.invoke(cli, ["issue", "edit-comment", "JIRA-1", "1"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Comment updated!\n"
    mo.update.assert_called_once_with(body="New comment")


def test_add_subtasks(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_issue.return_value.project.key = "JIRA"

    # Mock status
    mock_status_inprogress = MagicMock()
    mock_status_inprogress.return_value.id = "2"
    mock_status_inprogress.return_value.name = "In Progress"
    mock_status_inprogress.return_value.statusCategory.colorName = "yellow"
    # Mock issue
    mock_new_issue = MagicMock()
    mock_new_issue.return_value.key = "JIRA-2"
    mock_new_issue.return_value.fields.summary = "This is a new sub"
    mock_new_issue.return_value.fields.reporter = ""
    mock_new_issue.return_value.fields.assignee = ""
    mock_new_issue.return_value.fields.status = mock_status_inprogress()
    mock_jira.create_issue = MagicMock(return_value=mock_new_issue())
    mocker.patch(
        "click.edit", new=MagicMock(return_value="# Comment\nThis is a new sub|with desc"),
    )

    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    result = runner.invoke(cli, ["issue", "add-subtasks", "JIRA-1"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "JIRA-2 This is a new sub https://jira.local/browse/JIRA-2\n\n"


def test_rank_issue(mocker, runner, capfd):
    mock_jira = MagicMock()
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.update = MagicMock()
    mock_priority = MagicMock
    mock_priority.name = "Medium"
    mock_jira.priorities = MagicMock(return_value=[mock_priority()])
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    result = runner.invoke(cli, ["issue", "rank", "JIRA-1", "Medium"])

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "JIRA-1 ranked to Medium\n"
