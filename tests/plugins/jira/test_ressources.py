import pytest
from click.testing import CliRunner
from mock import MagicMock

from moic.base import cli


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


def test_project_succeeded(mocker, runner, capfd, tmp_path):

    mock_jira = MagicMock()
    mock_project = MagicMock()
    mock_project.return_value.key = "JIRA"
    mock_project.return_value.name = "My Test Project"
    mock_jira.projects = MagicMock(return_value=[mock_project()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    command_args = ["resources", "projects"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "My Test Project      : JIRA\n"


def test_issue_type_succeeded(mocker, runner, capfd, tmp_path):

    mock_jira = MagicMock()
    mock_issue_type = MagicMock()
    mock_issue_type.return_value.name = "Story"
    mock_issue_type.return_value.description = "Story default type"

    mock_jira.issue_types = MagicMock(return_value=[mock_issue_type()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    command_args = ["resources", "issue-type"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "Story                : Story default type\n"


def test_priorities_succeeded(mocker, runner, capfd, tmp_path):

    mock_jira = MagicMock()
    mock_priorities = MagicMock()
    mock_priorities.return_value.name = "High"
    mock_priorities.return_value.description = "23/19!!!!"

    mock_jira.priorities = MagicMock(return_value=[mock_priorities()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    command_args = ["resources", "priorities"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "High       : 23/19!!!!\n"


def test_status_succeeded(mocker, runner, capfd, tmp_path):

    mock_jira = MagicMock()
    mock_status = MagicMock()
    mock_status.return_value.name = "To Do"
    mock_status.return_value.description = "You have to do the taff"
    mock_status.return_value.raw = {"statusCategory": {"colorName": "blue"}}

    mock_jira.statuses = MagicMock(return_value=[mock_status()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    command_args = ["resources", "status"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert out == "To Do                     : You have to do the taff\n"
