import json

import pytest
from click.testing import CliRunner
from mock import MagicMock

from moic.base import cli
from moic.cli.utils import get_plugin_custom_commands


@pytest.fixture(scope="module")
def runner():
    return CliRunner()


@pytest.fixture(autouse=True)
def default_mock(mocker):
    mocker.patch("moic.plugins.jira.Instance.add_context")
    mocker.patch("moic.plugins.jira.Instance.setup_home_dir")

    # Add the custom from the plugin
    # This is done here because at modue moic.base load settings is empty and not mocked yet
    for c in get_plugin_custom_commands("jira"):
        cli.add_command(c)


def test_list(mocker, runner, capfd):

    mock_jira = MagicMock()
    # Mocking Boards
    mock_board = MagicMock()
    mock_board.return_value.id = 1
    mock_board.return_value.name = "Scrum Board"
    mock_boards = MagicMock()
    mock_boards.return_value = [mock_board()]
    mock_jira.boards = MagicMock(return_value=mock_boards())
    # Mocking Sprints
    mock_sprint = MagicMock()
    mock_sprint.return_value.id = 1
    mock_sprint.return_value.name = "Sprint 1"
    mock_sprint.return_value.state = "active"
    mock_sprint.return_value.goal = "Rule the world"
    mock_sprint.return_value.startDate = "2020-03-12T12:06:09.406+01:00"
    mock_sprint.return_value.endDate = "2020-03-25T12:06:00.000+01:00"
    mock_sprints = MagicMock()
    mock_sprints.return_value = [mock_sprint()]
    mock_jira.sprints = MagicMock(return_value=mock_sprints())
    mock_request = MagicMock()
    mock_request.return_value.status_code = 200
    mock_request.return_value.content = json.dumps(
        {
            "maxResults": 50,
            "startAt": 0,
            "total": 1,
            "isLast": True,
            "values": [
                {
                    "id": "1",
                    "self": "https://jira.local/rest/agile/1.0/board/131",
                    "name": "Scrum Board",
                    "type": "scrum",
                },
            ],
        }
    )
    mocker.patch("requests.get", new=MagicMock(return_value=mock_request()))
    mocker.patch("keyring.get_password")
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    # Mocking issues
    def raw_items(name):
        if name == "fields":
            return {"customfield_10000": "5.0"}
        return None

    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_issue.return_value.fields.status.statusCategory.key = "In Progress"
    mock_issue.return_value.raw.__getitem__.side_effect = raw_items
    mock_issue_done = MagicMock()
    mock_issue_done.return_value.key = "JIRA-2"
    mock_issue_done.return_value.fields.status.statusCategory.key = "done"
    mock_issue_done.return_value.raw.__getitem__.side_effect = raw_items
    mock_jira.search_issues = MagicMock(return_value=[mock_issue(), mock_issue_done()])
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    mock_track = MagicMock()
    mock_track.side_effect = lambda a, description: a
    mocker.patch("moic.plugins.jira.commands.sprint.base.track", new=mock_track)
    command_args = ["sprint", "list", "--project", "JIRA"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert (
        out
        == "Scrum Board (1)\n └─ (1) Sprint 1 ( 5.0 / 10.0 )\n    Goal  : Rule the world\n    Dates : 03/12/2020 - 03/25/2020\n"
    )


def test_get(mocker, runner, capfd):
    mock_jira = MagicMock()
    # Mocking Boards
    mock_board = MagicMock()
    mock_board.return_value.id = 1
    mock_board.return_value.name = "Scrum Board"
    mock_boards = MagicMock()
    mock_boards.return_value = [mock_board()]
    mock_jira.boards = MagicMock(return_value=mock_boards())
    mock_request = MagicMock()
    mock_request.return_value.status_code = 200
    mock_request.return_value.content = json.dumps(
        {
            "maxResults": 50,
            "startAt": 0,
            "total": 1,
            "isLast": True,
            "values": [
                {
                    "id": "1",
                    "self": "https://jira.local/rest/agile/1.0/board/131",
                    "name": "Scrum Board",
                    "type": "scrum",
                },
            ],
        }
    )
    mocker.patch("requests.get", new=MagicMock(return_value=mock_request()))
    mocker.patch("keyring.get_password")
    # Mocking Sprints
    mock_sprint = MagicMock()
    mock_sprint.return_value.id = 1
    mock_sprint.return_value.name = "Sprint 1"
    mock_sprint.return_value.state = "active"
    mock_sprint.return_value.goal = "Rule the world"
    mock_sprint.return_value.startDate = "2020-03-12T12:06:09.406+01:00"
    mock_sprint.return_value.endDate = "2020-03-25T12:06:00.000+01:00"
    mock_sprints = MagicMock()
    mock_sprints.return_value = [mock_sprint()]
    mock_jira.sprints = MagicMock(return_value=mock_sprints())
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    # Mocking status
    mock_status_open = MagicMock()
    mock_status_open.return_value.id = "1"
    mock_status_open.return_value.name = "To Do"
    mock_status_open.return_value.statusCategory.key = "to do"
    mock_status_open.return_value.statusCategory.colorName = "blue"
    mock_status_inprogress = MagicMock()
    mock_status_inprogress.return_value.id = "2"
    mock_status_inprogress.return_value.name = "In Progress"
    mock_status_inprogress.return_value.statusCategory.key = "intermediate"
    mock_status_inprogress.return_value.statusCategory.colorName = "yellow"
    mock_status_done = MagicMock()
    mock_status_done.return_value.id = "3"
    mock_status_done.return_value.name = "Done"
    mock_status_done.return_value.statusCategory.key = "done"
    mock_status_done.return_value.statusCategory.colorName = "green"
    mock_statuses = MagicMock()
    mock_statuses.return_value = [
        mock_status_open(),
        mock_status_inprogress(),
        mock_status_done(),
    ]
    mock_jira.statuses = MagicMock(return_value=mock_statuses())

    # Mocking issues
    def raw_items(name):
        if name == "fields":
            return {"customfield_10000": "5.0"}
        return None

    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_issue.return_value.fields.summary = "This is an issue"
    mock_issue.return_value.fields.issuetype.name = "Story"
    mock_issue.return_value.fields.status = mock_status_open()
    mock_issue.return_value.raw.__getitem__.side_effect = raw_items
    mock_issue_progress = MagicMock()
    mock_issue_progress.return_value.key = "JIRA-2"
    mock_issue.return_value.fields.summary = "This is an other issue"
    mock_issue.return_value.fields.issuetype.name = "Story"
    mock_issue_progress.return_value.fields.status = mock_status_inprogress()
    mock_issue_progress.return_value.raw.__getitem__.side_effect = raw_items
    mock_jira.search_issues = MagicMock(return_value=[mock_issue(), mock_issue_progress()])
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    mock_track = MagicMock()
    mock_track.side_effect = lambda a, description: a
    mocker.patch("moic.plugins.jira.commands.sprint.base.track", new=mock_track)
    command_args = ["sprint", "get", "--project", "JIRA"]
    result = runner.invoke(cli, command_args)

    out, err = capfd.readouterr()
    assert result.exit_code == 0
    assert (
        out
        == "\n(1) Sprint 1 : Rule the world\n\nTo Do\n └─ JIRA-1 This is an other issue https://jira.local/browse/JIRA-1\n"
    )
