from mock import MagicMock


def test_get_boards_succeed(mocker, tmp_path):
    mock_board = MagicMock()
    mock_board.return_value.name = "Board 1"
    mock_jira = MagicMock()
    mock_jira.boards = MagicMock(return_value=[mock_board()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    from moic.cli.completion import autocomplete_boards

    res = autocomplete_boards("", "", "Bo")
    assert res == [("Board 1")]


def test_get_sprints_succeed(mocker, tmp_path):
    mock_sprint = MagicMock()
    mock_sprint.return_value.id = "1"
    mock_sprint.return_value.name = "Sprint 1"
    mock_jira = MagicMock()
    mock_jira.sprints = MagicMock(return_value=[mock_sprint()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    from moic.cli.completion import autocomplete_sprints

    class Context:
        def __init__(self):
            self.params = {"project": "JIRA", "board": "board 1"}

    res = autocomplete_sprints(Context(), "", "Bo")
    assert res == [("1", "Sprint 1")]


def test_get_users_succeed(mocker, tmp_path):
    mock_user = MagicMock()
    mock_user.return_value.name = "ptilendier"
    mock_user.return_value.displayName = "Pierre Tilendier"
    mock_jira = MagicMock()
    mock_jira.search_users = MagicMock(return_value=[mock_user()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    from moic.cli.completion import autocomplete_users

    res = autocomplete_users("", "", "pti")
    assert res == [("ptilendier", "Pierre Tilendier")]


def test_get_projects_succeed(mocker, tmp_path):
    mock_project = MagicMock()
    mock_project.return_value.key = "JIRA"
    mock_project.return_value.name = "Jira Project"
    mock_jira = MagicMock()
    mock_jira.projects = MagicMock(return_value=[mock_project()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    from moic.cli.completion import autocomplete_projects

    res = autocomplete_projects("", "", "ji")
    assert res == [("JIRA", "Jira Project")]


def test_get_issue_types_succeed(mocker, tmp_path):
    mock_issue_type = MagicMock()
    mock_issue_type.return_value.name = "Story"
    mock_issue_type.return_value.description = "Jira Story type"
    mock_jira = MagicMock()
    mock_jira.issue_types = MagicMock(return_value=[mock_issue_type()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)
    from moic.cli.completion import autocomplete_issue_types

    res = autocomplete_issue_types("", "", "sto")
    assert res == [("Story", "Jira Story type")]


def test_get_transitions_succeed(mocker, tmp_path):
    mock_issue = MagicMock()
    mock_issue.return_value.key = "JIRA-1"
    mock_transition = MagicMock()
    trans_raw = {"name": "In Progress", "to": {"description": "Got to In Progress"}}
    mock_transition.return_value.__getitem__.side_effect = trans_raw.__getitem__
    mock_transition.return_value.name = "In Progress"

    mock_jira = MagicMock()
    mock_jira.issue = MagicMock(return_value=mock_issue())
    mock_jira.transitions = MagicMock(return_value=[mock_transition()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    from moic.cli.completion import autocomplete_transitions

    res = autocomplete_transitions("", ["issue", "move", "JIRA-1"], "In")
    assert res == [("In Progress", "Got to In Progress")]


def test_get_priorities_succeed(mocker, tmp_path):
    mock_priority = MagicMock()
    mock_priority.return_value.name = "Medium"
    mock_priority2 = MagicMock()
    mock_priority2.return_value.name = "Media"
    mock_jira = MagicMock()
    mock_jira.priorities = MagicMock(return_value=[mock_priority(), mock_priority2()])
    mocker.patch("moic.cli.base.CONF_DIR", new=tmp_path)
    mocker.patch("moic.plugins.jira.Instance.session", new=mock_jira)

    from moic.cli.completion import autocomplete_priorities

    res = autocomplete_priorities("", ["issue", "rank", "JIRA-1"], "Med")
    assert res == ["Medium", "Media"]
    res = autocomplete_priorities("", ["issue", "rank", "JIRA-1"], "Mediu")
    assert res == ["Medium"]
