# flake8: noqa
"""
Module for cli utils function
"""
from .base import (
    check_instance_is_up,
    get_plugin_autocomplete,
    get_plugin_command,
    get_plugin_custom_commands,
    get_plugin_instance,
    get_plugin_validator,
    get_template,
    print_comment,
    print_comments,
    print_issue,
    print_issues,
    print_status,
    sort_issue_per_status,
)
