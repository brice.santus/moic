# flake8: noqa
"""
Module for click commands autocompletion
"""
from .base import (
    autocomplete_boards,
    autocomplete_comments,
    autocomplete_contexts,
    autocomplete_issue_types,
    autocomplete_plugins,
    autocomplete_priorities,
    autocomplete_projects,
    autocomplete_sprints,
    autocomplete_transitions,
    autocomplete_users,
)
