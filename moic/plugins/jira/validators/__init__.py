# flake8: noqa
"""
Module for Moic cli arguments and options validators
"""
from .base import (
    validate_comment_id,
    validate_issue_key,
    validate_issue_type,
    validate_priority,
    validate_project_key,
    validate_user,
)
