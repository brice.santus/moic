moic.plugins.jira.commands.resources package
============================================

Submodules
----------

moic.plugins.jira.commands.resources.base module
------------------------------------------------

.. automodule:: moic.plugins.jira.commands.resources.base
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: moic.plugins.jira.commands.resources
   :members:
   :undoc-members:
   :show-inheritance:
