Plugins
=======

MOIC allows creation of plugins to interact with several issue tracker.
Today the only plugin setup is Jira. In a near future some other will be
integrated starting with Gitlab.

The creation of a plugin is quite simple.

In the future it will be simplified using `plugin name convention`_ to
unleashed plugin development

Create a plugin
---------------

Plugins should be created under ``moic/plugins/<plugin_name>``.

A plugin should provide several packages: \* **commands**: Contains
method called by Moic for each actions \* **completion**: Contains
autocompletion functions called by Moic \* **validators**: Contains data
validator functions called by Moic

Instance
~~~~~~~~

First of all a plugin should expose a Instance class which herited from
``MoicInstance``

.. code:: python

   from moic.cli import MoicInstance, console, global_settings, settings

   custom_commands = [] # use to list the custom commands which could be load by moic

   class Instance(MoicInstance):

       @property
       def session(self):
           """
           Store the session object which wrap tracker access and actions.
           It's also used to check the tracker connection access.
           """
           pass

       def create_session_instance(self) -> None:
           """
           This method is used to instanciate the session property if it doesn't exists yet
           """
           pass

       def add_context(self, name: str, force: bool = False) -> dict:
           """
           This method should build a dict contains the tracker configuration

           Returns:
               dict: The cont
           """
           pass

       def custom_config(self, project: str, force: bool = False) -> None:
           """
           This method could be use to add more specific configuration in the context
           using self.update_config(<dict>)
           """
           pass

Context
^^^^^^^

A context should minimaly get the following properties: \* **instance**:
Tracker url \* **login**: username to acces the tracker \*
**default_projet**: The default project id/key used by moic

Commands
~~~~~~~~

For each resources and default commands, Moic will try to load a
``module.method`` corresponding and pass it the options/arguments
provided by the user at runtime.

If a plugin, module or method doesn’t exists, Moic will fallback on a
warning

Example:

.. code:: python

   # if you call: "moic issue get" with the context using Jira plugin
   # Moic will try to import
   from moic.plugins.jira.commands.issue import get

Custom commands
^^^^^^^^^^^^^^^

If you need to add more specific method / resources than the basic one,
you can just add them in the ``custom_commands`` list exposed by your
plugin.

Moic could be able to import
``moic.plugins.<plugin_name>.custom_commands``

This commands should be ``click`` commands and will be directly imported
and exposed by Moic

Completion
~~~~~~~~~~

Moic provides autocompletion for basic resources but you need to
implement the way autocompletion retrieve results For eac

.. _plugin name convention: https://packaging.python.org/guides/creating-and-discovering-plugins/#using-naming-convention
