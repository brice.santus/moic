Configure
=========

Contexts
--------

Moic allows you to setup multiple contexts working with different
instances of issue trakers.

You can get the list of defined context with:

List and use contexts
~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

   > moic context list
   ✓ jira-cogip          : Jira Tracker of COGIP Inc
     gitlab              : Gitlab.com

You can switch easily from a context to another with:

.. code:: bash

   > moic context set gitlab
   Context swithed to gitlab

Create/delete a context
~~~~~~~~~~~~~~~~~~~~~~~

To create a new configured context, run:

.. code:: bash

   > moic context add <plugin_name> --name my_context --description "This is my new context"
   Context created

Moic will ask you some basic configuration (instance, login etc…). It
could store you password if needed within Keyring and will trigger the
plugin specific configuration.

bq. Today only **jira** plugin is available, other plugins will be
delivered soon (Gitlab, etc…)

You can delete a context with:

.. code:: bash

   > moic context delete my_context
   my_context deleted

Configuration file
------------------

The configuration is stored in a config file (default:
``~/.moic/config.yaml``). Here is an example with Jira plugin custom
configuration:

.. code:: yaml

   default:
     contexts:
     - custom_fields:
         peer: customfield_10800
         story_points: customfield_10106
       default_project: JIRA
       description: Jira Tracker of COGIP Inc
       instance: https://jira.cogip.fr
       login: bsantus
       name: jira-cogip
       plugin: jira
       projects:
         JIRA:
           workflow:
             done:
             - '10005'
             - '10001'
             indeterminate:
             - '10002'
             - '3'
             - '10007'
             - '10009'
             new:
             - '1'
             - '10000'
     current_context: jira-cogip
