moic.plugins.jira.completion package
====================================

Submodules
----------

moic.plugins.jira.completion.base module
----------------------------------------

.. automodule:: moic.plugins.jira.completion.base
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: moic.plugins.jira.completion
   :members:
   :undoc-members:
   :show-inheritance:
