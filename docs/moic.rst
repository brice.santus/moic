moic package
============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   moic.cli
   moic.plugins

Submodules
----------

moic.base module
----------------

.. automodule:: moic.base
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: moic
   :members:
   :undoc-members:
   :show-inheritance:
