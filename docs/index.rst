.. Moic documentation master file, created by
   sphinx-quickstart on Thu May 28 14:52:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Moic's documentation!
================================

   Freely inspired by https://pypi.org/project/jira-cli/

Command line utility to interact with issue manager such as Jira, Gitlab, etc...

**Highlights**

-  Modern CLI based on `Click`_ and `Rich`_
-  Context management
-  Multiple tracker plugin
-  Uniformed display

.. _Click: https://click.palletsprojects.com/en/7.x/
.. _Rich: https://github.com/willmcgugan/rich

Getting Started
---------------

-  Install moic

.. code:: bash

   > pip install moic

-  Configure moic

.. code:: bash

   > moic configure

-  Commands

.. code:: bash

   > moic
   Usage: moic [OPTIONS] COMMAND [ARGS]...

   Options:
     --help  Show this message and exit.

   Commands:
     config    Configure Jira cli, setting up instance, credentials etc...
     issue     Create, edit, list Jira issues
     rabbit    Print an amazing rabbit: Tribute to @fattibenji...
     resources List projects, issue_types, priorities, status
     sprint    Create, edit, list Jira Sprints
     template  List, edit templates
     version   Provide the current moic installed version

Autocompletion
--------------

To activate bash autocompletion just add: \* For bash

::

   # In your .bashrc
   eval "$(_MOIC_COMPLETE=source_bash moic)"

-  For zsh

::

   # In your .zshrc
   eval "$(_MOIC_COMPLETE=source_zsh moic)"

Contribute
----------

Feel free `open issues on Gitlab`_ or propose Merge Requests

Prerequisites
~~~~~~~~~~~~~

This project is based on `Poetry`_ as a package manager. It allows the
use of virtual environments and the lock of package versions, whether
they are in your dependencies or just sub-dependencies of these
dependencies.

Setup
~~~~~

-  Create virtualenv (Optionnaly you can use `pyenv`_ which is a Python
   Virtualenv Manager in combination with **Poetry**)

.. code:: bash

   poetry shell

-  Install dependencies

.. code:: bash

   poetry install

-  Install pre-commit (using `Pre-commit framework`_)

.. code:: bash

   pre-commit install

..

   Pre-commit will check isort, black, flake8

### Commit messages

This project uses semantic versioning so it is important to follow the
rules of `conventional commits`_.

.. _open issues on Gitlab: https://gitlab.com/brice.santus/moic/-/issues
.. _Poetry: https://python-poetry.org/docs/
.. _pyenv: https://github.com/pyenv/pyenv
.. _Pre-commit framework: https://pre-commit.com/
.. _conventional commits: https://www.conventionalcommits.org/


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   configure
   plugins
   moic
