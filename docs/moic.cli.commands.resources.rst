moic.cli.commands.resources package
===================================

Submodules
----------

moic.cli.commands.resources.base module
---------------------------------------

.. automodule:: moic.cli.commands.resources.base
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: moic.cli.commands.resources
   :members:
   :undoc-members:
   :show-inheritance:
